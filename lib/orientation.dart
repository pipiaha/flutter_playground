import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OrientationTestWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('orientation test'),
      ),
      body: Container(
        child: Center(
          child: RaisedButton(
            child: Text('landscape'),
            onPressed: () {
              showBottomSheet(
                  context: context,
                  builder: (context) {
                    return BottomSheetWidget(
                        MediaQuery.of(context).orientation);
                  });
            },
          ),
        ),
      ),
    );
  }
}

class BottomSheetWidget extends StatefulWidget {
  final Orientation orientation;

  BottomSheetWidget(this.orientation);

  @override
  State<StatefulWidget> createState() {
    return BottomSheetWidgetState(orientation);
  }
}

class BottomSheetWidgetState extends State<BottomSheetWidget> {
  Orientation orientation;
  MethodChannel methodChannel = MethodChannel('rotationChannel');

  BottomSheetWidgetState(this.orientation);

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    if (Platform.isIOS) {
      methodChannel.invokeListMethod('setLandscape');
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: Text(orientation.toString()),
    );
  }
}
