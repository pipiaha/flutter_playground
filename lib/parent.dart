import 'package:flutter/material.dart';

class ParentWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ParentState();
  }
}

class ParentState extends State<ParentWidget> {
  Map<String, Item> items;

  Orientation restore;

  @override
  void initState() {
    items = {};
    var m = {'a': 1, 'b': 2, 'c': 3};
    var itemList = m.entries.map((e) {
      return new Item(e.key, e.value);
    }).toList();
    itemList.forEach((i) {
      items[i.name] = i;
    });
  }

  List<Widget> _buildItems() {
    return this.items.values.map((item) {
      return _buildItem(item);
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('event pass'),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: <Widget>[]..addAll(_buildItems()),
        ),
      ),
    );
  }

  Widget _buildItem(Item item) {
    return Stack(
      children: <Widget>[
//        Container(
//          height: 50,
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[Text(item.name), Text(item.selected.toString())],
//          ),
//        ),
        ChildWidget2(
          item: item,
        ),
        Positioned.fill(
          child: new Material(
            color: Colors.transparent,
            child: new InkWell(
              onTap: () {
                setState(() {
                  bool select = this.items[item.name].selected;
                  this.items[item.name].selected = !select;
                });
              },
            ),
          ),
        ),
      ],
    );
  }
}

class ChildWidget extends StatelessWidget {
  Item item;

  ChildWidget({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[Text(item.name), Text(item.selected.toString())],
      ),
    );
  }
}

class Item {
  final String name;
  final int value;

  bool selected;

  Item(this.name, this.value, {this.selected = false});
}

class ChildWidget2 extends StatefulWidget {
  final Item item;

  const ChildWidget2({Key key, this.item}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ChildWidgetState(item);
  }
}

class ChildWidgetState extends State<ChildWidget2> {
  Item item;

  ChildWidgetState(this.item);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[Text(item.name), SelectWidget(item.selected)],
      ),
    );
  }
}

class SelectWidget extends StatelessWidget {
  final bool selected;

  SelectWidget(this.selected);

  @override
  Widget build(BuildContext context) {
    return Text(selected ? 'selected' : '--');
  }
}
